import time

# Modbus retries amount
MODBUS_RETRIES = 8

# Modbus safe and retry timeouts
MODBUS_TIMEOUT_SAFE = 0.05
MODBUS_TIMEOUT_RETRY = 0.05

# Safely writes a bit into a coil
def write(instrument, address, data):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      instrument.write_bit(address, data, functioncode = 0x05)
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")
  raise SystemExit

# Safely writes a multiple coils
def writes(instrument, address, data):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      instrument.write_bits(address, data)
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")
  raise SystemExit

# Safely read a coils
def read(instrument, address, size):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return instrument.read_bits(address, size)
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")
  raise SystemExit

# Safely reads a registers
def read_registers(instrument, address, size):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return instrument.read_registers(address, size, functioncode=0x4)
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")
  raise SystemExit


# Safely writes a register
def write_register(instrument, address, data):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return instrument.write_register(address, data, 0, 6)
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")
  #raise SystemExit

def write_register_signed(instrument, address, data):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return instrument.write_register(address, data, 0, 6, signed=True)
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol")

# Safely writes a register
def write_registers(instrument, address, data):
  for i in range(MODBUS_RETRIES):
    try:
      if i > 1:
        print ("ok")
      time.sleep(MODBUS_TIMEOUT_SAFE)
      return instrument.write_registers(address, data)
    except Exception:
      time.sleep(MODBUS_TIMEOUT_RETRY)

  print ("lol_lol")
  #raise SystemExit