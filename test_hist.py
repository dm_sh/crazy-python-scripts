from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from influxdb import InfluxDBClient
import statistics 
data = list(np.random.randint(2, size=15000))
client = InfluxDBClient(host='54.188.48.101', port=8086)

client.switch_database("geiger")
geiger_data = client.query('select elapsed(value, 10ms), first(value) FROM mqtt_consumer   WHERE time > now() - 3d')
data_size =  client.query('SELECT COUNT("value") FROM mqtt_consumer   WHERE time > now() - 3d ')
geiger_points = list(geiger_data.get_points(measurement='mqtt_consumer'))
data_size_l = list(data_size.get_points(measurement='mqtt_consumer'))

bits = []
mean_bits = []
four_bit_data = []
four_bit_bits = []
slide_bits = []
mean_bytes =[]

# for x in range(1, (data_size_l[0]['count'])-2):
   
#      if  (geiger_points[x]['elapsed']) < (geiger_points[x+1]['elapsed']):
#         bits.append(0)
#      elif(geiger_points[x]['elapsed']) > (geiger_points[x+1]['elapsed']):
#         bits.append(1)



myList = list(map(lambda x: x['elapsed'], geiger_points))[1:]

# myList = list(map(myFunc, geiger_points))[1:]
# def myFunc(element):
#     return element['elapsed']

#print(myList)

mean = np.mean(myList)
median = np.median(myList)

print("mean={0} median={1}".format(mean,median))

# for x in range(1, (data_size_l[0]['count'])-2, 2):
   
#      if  (geiger_points[x]['elapsed'] > 1530):
#         mean_bits.append(1)
#      elif(geiger_points[x]['elapsed'] < 1530):
#         mean_bits.append(0)

def myFunc(element):
    if (element < median):
        return 0
    if (element > median):
        return 1

mean_bits = list(filter(lambda x: x != None, list(map(myFunc, myList))))
# print(mean_bytes)
     
# print("bits=",bits)
# print("data=",data)

# print(data)

print("data_len=",len(data)//4)
for x in range(0, len(data)//4):
    bit_data = (data[-(1+4*x)]) | (data[-(2+4*x)] << 1) | (data[-(3+4*x)] << 2) | (data[-(4+4*x)] << 3)
    four_bit_data.append(bit_data)

# print("bits_len=",len(bits)//4)
# for x in range(0, len(bits)//4):
#     bit_data = (bits[-(1+4*x)]) | (bits[-(2+4*x)] << 1) | (bits[-(3+4*x)] << 2) | (bits[-(4+4*x)] << 3)
#     four_bit_bits.append(bit_data)

print("bits_len=",len(mean_bits)//4)
for x in range(0, len(mean_bits)//4):
    bit_data = (mean_bits[-(1+4*x)]) | (mean_bits[-(2+4*x)] << 1) | (mean_bits[-(3+4*x)] << 2) | (mean_bits[-(4+4*x)] << 3)
    mean_bytes.append(bit_data)



print("bits_len=",len(mean_bits)//4)
for x in range(0, len(mean_bits)//4):
    bit_data = (mean_bits[-(1+x)]) | (mean_bits[-(2+x)] << 1) | (mean_bits[-(3+x)] << 2) | (mean_bits[-(4+x)] << 3)
    slide_bits.append(bit_data)


delta_data = statistics.stdev(four_bit_data)

delta_slide_bits = statistics.stdev(slide_bits)
delta_mean_bytes = statistics.stdev(mean_bytes)

mean_data = np.mean(four_bit_data)
median_data = np.median(four_bit_data)


mean_mean = np.mean(mean_bytes)
median_mean = np.median(mean_bytes)
print("mean_data={0} median_data={1} mean_mean={2}  median_mean={3}".format(mean_data,median_data,mean_mean,median_mean))
print("delta_data={0} delta_mean_bytes={1} delta_slide_bits={2} ".format(delta_data,delta_mean_bytes,delta_slide_bits))

hist, bins = np.histogram(data, bins=16)
plt.subplot(311)
plt.hist(four_bit_data, 16)
plt.subplot(312)
plt.hist(mean_bytes,16)
plt.subplot(313)
plt.hist(slide_bits,16)
plt.show()