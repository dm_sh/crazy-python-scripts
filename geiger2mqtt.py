import minimalmodbus
import serial.tools.list_ports
import paho
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import time
import threading
import configparser
import os

COM               = "/dev/ttyAMA0"
slave_address     = 16
MODBUS_READ_INPUT = 0x4
REG_INPUT         = 51
MODBUS_MODE       = 'rtu'


prev_data = 0
dev = minimalmodbus.Instrument(COM, slave_address, mode=MODBUS_MODE)
dev.serial.baudrate = 115200
config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')
hostname = config.get('MQTT', 'hostname')
username = config.get('MQTT', 'username')
password = config.get('MQTT', 'password')

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = paho.mqtt.client.Client(client_id="", clean_session=True, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")
    #client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(hostname, port=1883)
    return client

def publish(client):
    global prev_data
    while True:

        data = dev.read_register(REG_INPUT, functioncode=MODBUS_READ_INPUT)
        if data != prev_data:
            prev_data = data
            client.publish("geiger/", payload=data, qos=0, retain=False)
            print(data)

def run():
    
    client = connect_mqtt()
    client.loop_start()
    publish(client)

run()

    
     

  



