import minimalmodbus
import serial.tools.list_ports
import time
import threading
from datetime import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import configparser
import os
import lol_functions
import telegram
import sys
import safeops
import random

print(sys.version)
from telegram.ext import Updater, CommandHandler

mb                     = None
MODBUS_PORT            = '/dev/ttyAMA0'
MODBUS_MODE            = 'rtu'
minimalmodbus.BAUDRATE = 115200
minimalmodbus.TIMEOUT  = 150
minimalmodbus.PARITY   = 'N'
slave_address          = 2
MODBUS_READ_INPUT      = 0x4
MODBUS_WRITE_HOLDING   = 0x6
REG_HOLDING_START           = 0x200
WERMA = 22
MU110_update = False
RGB_update = True
server_light_state = True
round_light_state = True
top_light_state = True
yellow_light_state = True
blobby_light_state = True
RGB_state = True
update_rc = False
werma_blink = False
geiger_pattern_on = False
green = 0
red = 1
yellow = 2
btn1 = 14
btn2 = 10
btn3 = 13
btn4 = 9
btn5 = 8 
btn6 = 7
grn_fix_btn_state = True
yell_fix_btn_state = True
mb1_addr = 1
mb2_addr = 2
mb3_addr = 56
dsp_addr = 3
MU110_addr = 16
SMI2_addr = 116
RGB_addr = 192


w_data = {'time': 11, '/seagull_ws/temperature/on': 11, '/seagull_ws/humidity/on': 11, '/seagull_ws/dew_point/on': 11} 

config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')

coil_rst = [item for item in range(0, 5+1)]
coil_rst2 = [item for item in range(6, 13+1)]

btn_light_state = [False for x in range(1, 8+1)]

buttons = [item for item in range(0x308, 0x30D+1)]
buttons2 = [item for item in range(782, 789+1)]
btn_state = [0 for x in range(6)]
RGB_data = [0 for x in range(4)]
btn_state2 = [0 for x in range(6, 13+1)]
display_mode = 0
MODE_NUMBER = 5
mode = 2
werma_state = False
werma_update = True
update_disp = True
display_mode_SMI = 0
update_disp_SMI = False
state1 = False

# def safe_read_registers(mb, address, size):
#   for i in range(RETRIES):
#     try:
#       if i > 1:
#         print("retry read", i)
#       time.sleep(SAFE_TIMEOUT)
#       return mb.read_registers(address, size, functioncode=0x4)
#     except Exception:
#       print "except"
#       time.sleep(RETRY_TIMEOUT)

#   print("modbus TIMEOUT")



class Blinker:
  def __init__(self):
    self.LED_arr = [(1 if (i%2==0) else 0) for i in range(24)] 
    self.curr_index = 0
    self.dir = 0
    self.mode = 3
    self.update = False
    
  def set_mode(self, mode):
    self.curr_index = 0
    self.mode = mode
    self.dir = 0
    self.LED_arr = [0 for i in range(24)]
    self.update = True

  def process(self):
    if self.mode == 0:
      self.mega_fun()
    elif self.mode == 1:
      self.mega_fun2()
    elif self.mode == 2:
      self.mega_off()
    elif self.mode == 3:
      self.just_all_on()
    elif self.mode == 4:
      self.geiger_pattern()

  def mega_fun(self):
    self.LED_arr = [(1 if (i%2==0) else 0) for i in range(24)]
    self.update = True
    time.sleep(0.5)
    self.LED_arr = [(0 if (i%2==0) else 1) for i in range(24)]
    self.update = True
    time.sleep(0.5)

  def geiger_pattern(self):
    global geiger_pattern_on
    if geiger_pattern_on:
      self.LED_arr = [random.getrandbits(1) for i in range(24)]
      geiger_pattern_on = False
      self.update = True

  def just_all_on(self):
    self.LED_arr = [1 for i in range(24)]
    self.update = True


  def mega_fun2(self):
    if self.dir == 1:
      self.LED_arr[self.curr_index] = 0
    else:
      self.LED_arr[self.curr_index] = 1

    self.curr_index += 1
    if self.curr_index >= 24:
      self.curr_index = 0
      if (self.dir == 1):
        self.dir = 0
      else:
        self.dir = 1

    self.update = True
    time.sleep(0.2)

  def mega_off(self):
    
    self.LED_arr = [0 for i in range(24)]
    self.update = True

blinker = Blinker()

def bus_deals():
  global blinker
  global update_disp
  global MU110_update
  global RGB_update
  global RGB_state
  global server_light_state
  global round_light_state 
  global SMI2
  global RGB
  global RGB_data
  global update_rc
  global btn_light_state
  global update_disp_SMI
  global display_mode_SMI
  global yellow_light_state
  global blobby_light_state
  while True:
    if update_disp:
      if display_mode == 0:
        data2disp(datetime.now().time().strftime('%H.%M'), True, False,False)

      elif display_mode == 1:
        temp = (w_data['/seagull_ws/temperature/on'])
        
       # data2disp(-5, True, True,False)
       
        
      elif display_mode == 2:
        data2disp(datetime.now().date().strftime('%d.%m'), True, True,True)
       
       
      update_disp = False
    

    if update_disp_SMI:
      update_disp_SMI = False
      if display_mode_SMI == 0:
        safeops.write_register(SMI2, 18, 3)
        display_data_int = int((float(datetime.now().time().strftime('%H.%M'))*100))
        
        safeops.write_register(SMI2, 25, display_data_int)

        

      elif display_mode_SMI == 1:
        temp = (w_data['/seagull_ws/temperature/on'])
        safeops.write_register(SMI2, 18, 2)
        display_data_int = int((float(temp)*10))
        print(display_data_int)
        safeops.write_register_signed(SMI2, 25, display_data_int)

    if werma_update:
      mb2.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_7, werma_state, 0, MODBUS_WRITE_HOLDING)

    if update_rc:


      if btn_light_state[1]:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_1, 1, 0, MODBUS_WRITE_HOLDING)
      else:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_1, 0, 0, MODBUS_WRITE_HOLDING)

      if btn_light_state[2]:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_2, 1, 0, MODBUS_WRITE_HOLDING)
      else:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_2, 0, 0, MODBUS_WRITE_HOLDING)

      if btn_light_state[3]:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_3, 1, 0, MODBUS_WRITE_HOLDING)
      else:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_3, 0, 0, MODBUS_WRITE_HOLDING)

      if btn_light_state[6]:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_4, 1, 0, MODBUS_WRITE_HOLDING)
      else:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_4, 0, 0, MODBUS_WRITE_HOLDING)

      if btn_light_state[7]:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_5, 1, 0, MODBUS_WRITE_HOLDING)
      else:
        mb3.write_register(lol_functions.REG_HOLDING_SET_VALUE_UIO_5, 0, 0, MODBUS_WRITE_HOLDING)

      update_rc = False


    if RGB_update:
      if RGB_state:
        #safeops.write_register(RGB, red, random.randint(0, 254))
        safeops.write_register(RGB, red, RGB_data[0])
        safeops.write_register(RGB, green, RGB_data[1])
        safeops.write_register(RGB, yellow, RGB_data[2])
        
        
      else:
        safeops.write_register(RGB, red, 0)
        safeops.write_register(RGB, yellow, 0)
        safeops.write_register(RGB, green, 0)
      RGB_update = False

    if blinker.update:
      mb1.write_registers(lol_functions.REG_HOLDING_SET_VALUE_UIO_1, blinker.LED_arr[:16])
      mb2.write_registers(lol_functions.REG_HOLDING_SET_VALUE_UIO_9, blinker.LED_arr[16:])
      
      
      blinker.update = False

    if MU110_update:
      if round_light_state :
        MU110.write_register(1, 1000, 0, 16)
   
      else :
        MU110.write_register(1, 0, 0, 16)

      if server_light_state :
        MU110.write_register(0, 1000, 0, 16)
      else :
        MU110.write_register(0, 0, 0, 16)

      if top_light_state :
        MU110.write_register(2, 1000, 0, 16)
      else :
        MU110.write_register(2, 0, 0, 16)

      if blobby_light_state :
        MU110.write_register(3, 1000, 0, 16)
      else :
        MU110.write_register(3, 0, 0, 16)

      if yellow_light_state :
        MU110.write_register(4, 1000, 0, 16)
      else :
        MU110.write_register(4, 0, 0, 16)

      MU110_update = False

    for x in range(0, 2):
      if get_state(x):
        btn_state[x] = 1

    for x in range(0, 4):
      if get_state2(x):
        btn_state2[x] = 1

    for x in range(6, 8):
      if get_state2(x):
        btn_state2[x] = 1

def connect(addres):
  dev = minimalmodbus.Instrument(MODBUS_PORT, addres, mode=MODBUS_MODE)
  dev.serial.baudrate = 115200
  if not dev.serial.is_open:
    try:
      dev.serial.open()
      print('{0} connection established'.format(MODBUS_PORT))
    except:
      print('{0} connection error'.format(MODBUS_PORT))
  return dev


def board_config1():
  global mb1
  try:
    #config leds
    mb1.write_registers(lol_functions.REG_HOLDING_CNFG_UIO_1, [lol_functions.UIO_MODE_OUTPUT for x in range(16)])
    print('Board is configured')
  except:
    print('MODBUS communication error')


def board_config2():
  global mb2
  try:
    mb2.write_registers(lol_functions.REG_HOLDING_CNFG_UIO_1, [lol_functions.UIO_MODE_INPUT_FALLING for x in range(6)])
    mb2.write_registers(lol_functions.REG_HOLDING_CNFG_UIO_7, [lol_functions.UIO_MODE_OUTPUT for x in range(10)])
    print('Board is configured')
  except:
    print('MODBUS communication error')


def board_config3():
  global mb3
  try:
    #config leds
    mb3.write_registers(lol_functions.REG_HOLDING_CNFG_UIO_1, [lol_functions.UIO_MODE_OUTPUT for x in range(6)])
    mb3.write_registers(lol_functions.REG_HOLDING_CNFG_UIO_7, [lol_functions.UIO_MODE_INPUT_FALLING for x in range(8)])

    print('Board is configured')
  except:
    print('MODBUS communication error')


def get_state(btn):
  global mode
  if mb2.read_register(buttons[btn], number_of_decimals=0, functioncode=MODBUS_READ_INPUT):
      mb2.write_bit(coil_rst[btn],1,functioncode=5)
      print ("btn{0} pressed".format(btn))
      return True
  else:
      return False

def get_state2(btn):
  global mode
  global grn_fix_btn_state
  global yell_fix_btn_state
  global update_rc





  if mb3.read_register(buttons2[btn], number_of_decimals=0, functioncode=MODBUS_READ_INPUT):
      mb3.write_bit(coil_rst2[btn],1,functioncode=5)
      print ("btn{0} pressed".format(btn))
      return True
  else:
      return False

def process_btns():
  global werma_blink
  global werma_state
  global blinker
  global mode
  global display_mode
  global update_disp
  global RGB_state
  global RGB_update
  global RGB_data
  global update_rc
  global btn_light_state
  global MU110_update
  global round_light_state
  global top_light_state
  global blobby_light_state
  global yellow_light_state
  if btn_state[1]:
    werma_state = not werma_state
    btn_state[1] = False
    blinker.update = True
    print ("lol")

  if btn_state[0]:
    btn_state[0] = False
    blinker.update = True
    blinker.set_mode(mode)
    print ("mode {0} ".format(mode))
    mode = mode + 1
    if mode >= MODE_NUMBER:
      mode = 0

  if btn_state[2]:
    display_mode = display_mode + 1
    if display_mode > 2:
      display_mode = 0    
    btn_state[2] = False
    update_disp = True

  if btn_state[3]:
    
    btn_state[3] = False
    RGB_state = True
    RGB_update = True

  if btn_state[4]:
    
    btn_state[4] = False
    RGB_state = False
    RGB_update = True

  if btn_state2[0]:
    round_light_state = not round_light_state
    MU110_update = True
    #werma_blink = not werma_blink
    btn_state2[0] = False
    update_rc = True
    print ("lol0")

  if btn_state2[1]:
    btn_light_state[1] = not btn_light_state[1]
    #werma_blink = not werma_blink
    btn_state2[1] = False
    update_rc = True
    
    if btn_light_state[7]:
      blobby_light_state = btn_light_state[1]
      MU110_update = True
    else:
      if btn_light_state[1]:
        RGB_data[0] = 255
      else:
        RGB_data[0] = 0
      RGB_update = True
    print ("lol1")

  if btn_state2[2]:
    #werma_state = not werma_state
    btn_light_state[2] = not btn_light_state[2]
    btn_state2[2] = False
    update_rc = True
    if btn_light_state[7]:
      yellow_light_state = btn_light_state[2]
      MU110_update = True
    else:
      if btn_light_state[2]:
        RGB_data[2] = 255
      else:
        RGB_data[2] = 0
      RGB_update = True
        
    print ("lol2")

  if btn_state2[3]:
    btn_light_state[3] = not btn_light_state[3]
    #werma_blink = not werma_blink
    btn_state2[3] = False
    update_rc = True
    if btn_light_state[7]:
      top_light_state = btn_light_state[3]
      MU110_update = True
    else:
      if btn_light_state[3]:
        RGB_data[1] = 255
      else:
        RGB_data[1] = 0
      RGB_update = True
    print ("lol3")
    
  if btn_state2[6]:
    btn_light_state[6] = not btn_light_state[6]
    #werma_blink = not werma_blink
    btn_state2[6] = False
    update_rc = True
    print ("lol_btn_light_state[6]")

  if btn_state2[7]:
    btn_light_state[7] = not btn_light_state[7]
    #werma_blink = not werma_blink
    btn_state2[7] = False
    update_rc = True
    print ("lol_btn_light_state[7]")


def sw_display():
  global display_mode
  global update_disp
  display_mode = display_mode + 1
  if display_mode > 2:
    display_mode = 0    
  update_disp = True
  threading.Timer(2, sw_display).start()   

def sw_display_SMI():
  
  global display_mode_SMI
  global update_disp_SMI
  display_mode_SMI = display_mode_SMI + 1
  if display_mode_SMI > 1:
    display_mode_SMI = 0    
  update_disp_SMI = True
  threading.Timer(2, sw_display_SMI).start()  

def update_RGB():
  global RGB_update
  global RGB_state
  global grn_fix_btn_state
  #if grn_fix_btn_state:
  #  RGB_state = True
  #  RGB_update = True
    
  #threading.Timer(5, update_RGB).start() 

def update_werma():
  global werma_update
  global werma_state
  global werma_blink

  werma_update = True
  if werma_blink:
    werma_state = not werma_state
    
  threading.Timer(0.5, update_werma).start() 

def fun():
  global blinker
  while True:
    process_btns()
    blinker.process()

def data2disp(display_data, LED1, LED2, LED3):


  data = {
    "led1": LED1,
    "led2": LED2,
    "led3": LED3,
    "char1": { "value": 1, "dotted": False },
    "char2": { "value": 2, "dotted": True },
    "char3": { "value": 3, "dotted": False },
    "char4": { "value": 4, "dotted": False }
  }
  display_data_int = int((float(display_data)*100))
  data["char1"]["value"] = int(display_data_int // 1000)
  data["char2"]["value"] = int((display_data_int / 100 )%10)
  data["char3"]["value"] = int((display_data_int / 10)%10)
  data["char4"]["value"] = int(display_data_int % 10)

  if data["char1"]["dotted"]:
    data1 = data["char1"]["value"] + 0x8000
  else:
    data1 = data["char1"]["value"]   
 
  if data["char2"]["dotted"]:
    data2 = data["char2"]["value"] + 0x8000
  else:
    data2 = data["char2"]["value"]

  if data["char3"]["dotted"]:
    data3 = data["char3"]["value"] + 0x8000
  else:
    data3 = data["char3"]["value"]

  if data["char4"]["dotted"]:
    data4 = data["char4"]["value"] + 0x8000
  else:
    data4 = data["char4"]["value"]

  dsp.write_register(0x220, data1, 0, MODBUS_WRITE_HOLDING)
  dsp.write_register(0x221, data2, 0, MODBUS_WRITE_HOLDING)
  dsp.write_register(0x222, data3, 0, MODBUS_WRITE_HOLDING)
  dsp.write_register(0x223, data4, 0, MODBUS_WRITE_HOLDING)

  dsp.write_bit(0x10, not data['led1'], functioncode=5)
  dsp.write_bit(0x11, not data['led2'], functioncode=5)
  dsp.write_bit(0x12, not data['led3'], functioncode=5)

def print_mqqt_data(client, userdata, message):
    #global users
    w_data['time'] = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    w_data[message.topic] = float(message.payload.decode("utf-8"))
    
    
global display_data

mb1 = connect(mb1_addr)
mb2 = connect(mb2_addr)
mb3 = connect(mb3_addr)
dsp = connect(dsp_addr)
MU110 = connect(MU110_addr)
SMI2 = connect(SMI2_addr)
RGB = connect(RGB_addr)


print(datetime.now().time().strftime('%H.%M'))

board_config1()
board_config2()
board_config3()
safeops.write_register(SMI2, 17, 0)
sw_display()
sw_display_SMI()
update_RGB()
update_werma()

def server_light(bot, update):
    global server_light_state
    global MU110_update
    global RGB_state
    global RGB_update
    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    
    RGB_state = True
    RGB_update = True
    server_light_state = not server_light_state
    msg = server_light_state
    MU110_update = True
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,reply_markup=reply_markup)


def round_light(bot, update):
    global round_light_state
    global MU110_update
    global RGB_state
    global RGB_update
    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    
    RGB_state = True
    RGB_update = True
    round_light_state = not round_light_state
    
    msg = round_light_state
    MU110_update = True
    
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg)


def top_light(bot, update):
    global top_light_state
    global MU110_update
    global RGB_state
    global RGB_update
    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    
    RGB_state = True
    RGB_update = True
    top_light_state = not top_light_state
    msg = top_light_state
    MU110_update = True
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg)

def blobby_light(bot, update):
    global blobby_light_state
    global MU110_update

    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)

    blobby_light_state = not blobby_light_state
    msg = blobby_light_state
    MU110_update = True
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,reply_markup=reply_markup)


def yellow_light(bot, update):
    global yellow_light_state
    global MU110_update

    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    

    yellow_light_state = not yellow_light_state
    msg = yellow_light_state
    MU110_update = True
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg,reply_markup=reply_markup)

def off(bot, update):

    global RGB_state
    global RGB_update
    custom_keyboard = [["/server_light"],["/round_light"],["/top_light"],["/off"],["/blobby_light"],["/yellow_light"]]
    reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
    
    RGB_state = False
    RGB_update = True
    msg = "lol"

    
    print(update.message.chat_id, update.message.text)  
    bot.send_message(chat_id=update.message.chat_id,
                     text=msg)


def bot():
    updater = Updater(token='492607821:AAGHboeWC7NC4UfQtVUBvSiZDEDhPfpF6eo')
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("server_light", server_light))
    dp.add_handler(CommandHandler("round_light", round_light))
    dp.add_handler(CommandHandler("top_light", top_light))
    dp.add_handler(CommandHandler("off", off))
    dp.add_handler(CommandHandler("blobby_light", blobby_light))
    dp.add_handler(CommandHandler("yellow_light", yellow_light))
    updater.start_polling()


def subs():
    hostname = config.get('MQTT', 'hostname')
    username = config.get('MQTT', 'username')
    password = config.get('MQTT', 'password')
    print(hostname)
    subscribe.callback(print_mqqt_data, '/seagull_ws/temperature/+', qos=0, userdata=None, hostname=hostname,
    client_id="", keepalive=60, will=None, tls=None,
    protocol=mqtt.MQTTv311)


def geiger_mqtt(client, userdata, message):
  global geiger_pattern_on
  print('geiger_lol1')
  geiger_pattern_on = True

def geiger():
    print('geiger_lol3')
    test = subscribe.callback(geiger_mqtt, '#', qos=0, userdata=None, hostname='54.188.48.101',
    client_id="", keepalive=60, will=None, tls=None,
    protocol=mqtt.MQTTv311)
    print(test)
    

dt = datetime.now()
dt.hour
dt.second

t = threading.Thread(name='bus_deals', target=bus_deals)
w = threading.Thread(name='fun', target=fun)
s = threading.Thread(name='subs', target=subs)
b = threading.Thread(name='bot', target=bot)
x = threading.Thread(name='geiger', target=geiger)

w.start()
t.start()
s.start()
b.start()
x.start()
