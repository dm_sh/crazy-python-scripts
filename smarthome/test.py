import minimalmodbus
import serial.tools.list_ports
import time
import threading
from datetime import datetime
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import configparser
import os
import lol_functions
import telegram
import sys
print(sys.version)
from telegram.ext import Updater, CommandHandler


mb                     = None
MODBUS_PORT            = '/dev/ttyAMA0'
MODBUS_MODE            = 'rtu'

minimalmodbus.TIMEOUT  = 50
minimalmodbus.PARITY   = 'N'
slave_address          = 116
MODBUS_READ_INPUT      = 0x4
MODBUS_WRITE_HOLDING   = 0x6
REG_HOLDING_START           = 0x200
global i 


def connect(addres):
  dev = minimalmodbus.Instrument(MODBUS_PORT, addres, mode=MODBUS_MODE)
  dev.serial.baudrate = 115200
  if not dev.serial.is_open:
    try:
      dev.serial.open()
      print('{0} connection established'.format(MODBUS_PORT))
    except:
      print('{0} connection error'.format(MODBUS_PORT))
  return dev

#SMI2 = connect(116)

def main():
    SMI2 = connect(116)
    time.sleep(1)
    # SMI2.write_register( 17, 2)
    # time.sleep(1)
    
    # time.sleep(1)
    SMI2.write_register( 25, -5, signed=True)
    time.sleep(1)


if __name__ == "__main__":
    main()