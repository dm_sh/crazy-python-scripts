
import minimalmodbus
import serial.tools.list_ports
import time
import threading
import random
import numpy as np
import matplotlib.pyplot as plt
from influxdb import InfluxDBClient

COM               = "/dev/ttyUSB0"
slave_address     = 7

#serial port for Arkadii
candle_dev = minimalmodbus.Instrument(COM, slave_address, mode='rtu')
candle_dev.serial.baudrate = 115200

#serial port for display
disp = serial.Serial('/dev/ttyUSB1')

#data base client
client = InfluxDBClient(host='weather.shsh.io', port=8086)
client.switch_database("sensors")


def off_all_candles():
    candle_dev.write_bit(8,0, functioncode=5)
    candle_dev.write_bit(9,0, functioncode=5)
    candle_dev.write_bit(10,0, functioncode=5)
    candle_dev.write_bit(11,0, functioncode=5)
    candle_dev.write_bit(12,0, functioncode=5)
    candle_dev.write_bit(13,0, functioncode=5)
    candle_dev.write_bit(14,0, functioncode=5)
    candle_dev.write_bit(15,0, functioncode=5)


def enable_all_candles():
    candle_dev.write_bit(0,1, functioncode=5)
    candle_dev.write_bit(1,1, functioncode=5)
    candle_dev.write_bit(2,1, functioncode=5)
    candle_dev.write_bit(3,1, functioncode=5)
    candle_dev.write_bit(4,1, functioncode=5)
    candle_dev.write_bit(5,1, functioncode=5)
    candle_dev.write_bit(6,1, functioncode=5)
    candle_dev.write_bit(7,1, functioncode=5)


def generate_data():
    geiger_data = client.query('select elapsed(value, 10ms) FROM mqtt WHERE ("topic" = \'geiger/\') and  time > now() - 120s')

    geiger_points = [x['elapsed'] for x in geiger_data.get_points(measurement='mqtt')]

    median = np.median(geiger_points)
    print(f'Median: {median}')

    data = []
    bitn = 0
    byte = 0
    for bit in map(lambda x: 0 if x < median else 1, geiger_points):
        byte |= bit << bitn
        bitn += 1
        if bitn == 8:
            data.append(byte)
            bitn = 0
            byte = 0
    print(data)

    rand = data[random.randint(0, len(data) - 1)]

    #display value
    dispstr = '\r{}\r{}{}'.format(" " * 13, " " * 9, rand)
    disp.write(bytes(dispstr, 'utf-8'))

    #enable candle
    off_all_candles()
    regn = abs(rand // 32 - 7) + 8
    candle_dev.write_bit(regn, 1, functioncode=5)

    # hist, bins = np.histogram(data, bins=8)
    # plt.hist(data, 8)
    # plt.show()
  

enable_all_candles()

while(True):
    generate_data()
    time.sleep(5)
