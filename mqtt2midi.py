import paho
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import time
import threading
import configparser
import os
import rtmidi
import threading
import random


midiout = rtmidi.MidiOut()
available_ports = midiout.get_ports()
print(available_ports)
if available_ports:
    midiout.open_port(1)
else:
    midiout.open_virtual_port("My virtual output")


prev_data = 0

DEVICE_COUNT = 10

pressed = [None] * DEVICE_COUNT

NOTES = [
    1, -30, 7, 11,
    2, 4, -23, 13,
    10, 6, 12, 14]

NOTES = [x + 60 for x in NOTES]


def subs():

    print('geiger_lol4')

    test = subscribe.callback(print_mqqt_data, 'geiger/+', qos=0, userdata=None, hostname='localhost',
    client_id="", keepalive=60, will=None, tls=None,
    protocol=mqtt.MQTTv311)


def print_mqqt_data(client, userdata, message):
    data = int(message.payload.decode("utf-8"))
    print(data)
    
    rand = random.randint(1,12)

    print(data)

    note_on = None
    note_off = None

    for i in range(DEVICE_COUNT):
        if(data & (1 << i)):
            pressed[i] = NOTES[i]
            note_on = [0x90, pressed[i], rand*10]
            midiout.send_message(note_on)
            print("on", note_on)
        else:
            if pressed[i] != None:
                note_off = [0x80, pressed[i], 0]
                print("off", note_off)
                midiout.send_message(note_off)
                pressed[i] = None


s = threading.Thread(name='subs', target=subs)

s.start()

    
     

  



