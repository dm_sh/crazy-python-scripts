import minimalmodbus
import serial.tools.list_ports
import time
import configparser
import paho
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import os
import safeops
COM               = "/dev/ttyS0"

humi_temp_addr     = 15
a_in_addr          = 14
smi_addr           = 16
wind_sns_addr      = 3
wind_dir_sns_addr  = 2
illumi_sns_addr    = 1

SMI_DATA_REG     = 25
WIND_ADC_REG     = 0x100
TEMP_REG         = 0x0102 
HUMI_REG         = 0x0103
WIND_SNS_REG     = 0
WIND_SNS_DIR_REG = 0
ILLUMI_SNS_REG = 0x17

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val 


config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')

hostname = config.get('MQTT', 'hostname')
username = config.get('MQTT', 'username')
password = config.get('MQTT', 'password')


def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)
    client = paho.mqtt.client.Client(client_id="", clean_session=True, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")
    #client = mqtt_client.Client(client_id)
    #client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(hostname, port=1883)
    return client


def publish(client):
    
    while True:
        time.sleep(0.2)
        wind_speed = (safeops.read_register(wind_sns, WIND_SNS_REG))/10
        time.sleep(0.2)
        wind_dir = safeops.read_register(wind_dir_sns, WIND_SNS_DIR_REG)
        time.sleep(0.2)
        temp_temp = (safeops.read_register(humi_temp, TEMP_REG))
        temp = ( twos_comp(temp_temp,16)) / 100
        humi = (safeops.read_register(humi_temp, HUMI_REG)) / 100
        time.sleep(0.2)
        illumi = safeops.read_register(illumi_sns, ILLUMI_SNS_REG)
        wind_volts = WIND_ADC2VOLTS_COEFF * safeops.read_register(a_in, WIND_ADC_REG)

        print('wind_speed:{0:.2f}, wind_dir:{1:.0f}, temp:{2:.2f}, humi:{3:.2f}, illumi:{4:.0f}, wind_volts:{5:.2f}'.format(wind_speed,wind_dir,temp,humi,illumi,wind_volts))
        time.sleep(0.2)

        safeops.write_register(smi, 18,2)
        safeops.write_register_signed(smi, SMI_DATA_REG,temp*10)

        client.publish("lighthouse/wind_speed", payload=wind_speed, qos=0, retain=False)
        client.publish("lighthouse/wind_dir", payload=wind_dir, qos=0, retain=False)
        client.publish("lighthouse/temp", payload=temp, qos=0, retain=False)
        client.publish("lighthouse/humi", payload=humi, qos=0, retain=False)
        client.publish("lighthouse/illumi", payload=illumi, qos=0, retain=False)
        client.publish("lighthouse/wind_volts", payload=wind_volts, qos=0, retain=False)
        time.sleep(5)


def run():
    client = connect_mqtt()
    client.loop_start()
    publish(client)



# client = paho.mqtt.client.Client(client_id="", clean_session=True, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")

# client.username_pw_set(username, password)
# res = client.connect(hostname, port=1883, keepalive=60, bind_address="")
# print(res)



if __name__ == '__main__':
    MODBUS_MODE       = 'rtu'

    WIND_ADC2VOLTS_COEFF = 0.005

    humi_temp = minimalmodbus.Instrument(COM, humi_temp_addr, mode=MODBUS_MODE)
    humi_temp.serial.baudrate = 9600
    humi_temp.serial.timeout = 0.5

    a_in = minimalmodbus.Instrument(COM, a_in_addr, mode=MODBUS_MODE)
    a_in.serial.baudrate = 9600
    a_in.serial.timeout = 0.5

    smi = minimalmodbus.Instrument(COM, smi_addr, mode=MODBUS_MODE)
    smi.serial.baudrate = 9600
    smi.serial.timeout = 0.5

    wind_sns = minimalmodbus.Instrument(COM, wind_sns_addr, mode=MODBUS_MODE)
    wind_sns.serial.baudrate = 9600
    wind_sns.serial.timeout = 1

    wind_dir_sns = minimalmodbus.Instrument(COM, wind_dir_sns_addr, mode=MODBUS_MODE)
    wind_dir_sns.serial.baudrate = 9600
    wind_dir_sns.serial.timeout = 1

    illumi_sns = minimalmodbus.Instrument(COM, illumi_sns_addr, mode=MODBUS_MODE)
    illumi_sns.serial.baudrate = 9600
    illumi_sns.serial.timeout = 1
    run()


# while(1):
#     time.sleep(0.2)
#     wind_speed = (safeops.read_register(wind_sns, WIND_SNS_REG))/10
#     time.sleep(0.2)
#     wind_dir = safeops.read_register(wind_dir_sns, WIND_SNS_DIR_REG)
#     time.sleep(0.2)
#     temp_temp = (safeops.read_register(humi_temp, TEMP_REG))
#     temp = ( twos_comp(temp_temp,16)) / 100
#     humi = (safeops.read_register(humi_temp, HUMI_REG)) / 100
#     time.sleep(0.2)
#     illumi = safeops.read_register(illumi_sns, ILLUMI_SNS_REG)
#     wind_volts = WIND_ADC2VOLTS_COEFF * safeops.read_register(a_in, WIND_ADC_REG)
    
#     print('wind_speed:{0:.2f}, wind_dir:{1:.0f}, temp:{2:.2f}, humi:{3:.2f}, illumi:{4:.0f}, wind_volts:{5:.2f}'.format(wind_speed,wind_dir,temp,humi,illumi,wind_volts))
#     time.sleep(0.2)

#     safeops.write_register(smi, 18,2)
#     safeops.write_register_signed(smi, SMI_DATA_REG,temp*10)

#     client.publish("lighthouse/wind_speed", payload=wind_speed, qos=0, retain=False)
#     client.publish("lighthouse/wind_dir", payload=wind_dir, qos=0, retain=False)
#     client.publish("lighthouse/temp", payload=temp, qos=0, retain=False)
#     client.publish("lighthouse/humi", payload=humi, qos=0, retain=False)
#     client.publish("lighthouse/illumi", payload=illumi, qos=0, retain=False)
#     client.publish("lighthouse/wind_volts", payload=wind_volts, qos=0, retain=False)
#     time.sleep(5)



