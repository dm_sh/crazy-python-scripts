import minimalmodbus
import serial.tools.list_ports
import time
import configparser
import paho
import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt
import os
COM               = "/dev/ttyAMA0"
slave_address     = 16
MODBUS_READ_INPUT = 0x4
REG_INPUT         = 51
MODBUS_MODE       = 'rtu'

dev = minimalmodbus.Instrument(COM, slave_address, mode=MODBUS_MODE)
dev.serial.baudrate = 115200

config  = configparser.ConfigParser()
currDir = os.path.dirname(os.path.realpath(__file__))
config.read(currDir + '/config.cfg')

hostname = config.get('MQTT', 'hostname')
username = config.get('MQTT', 'username')
password = config.get('MQTT', 'password')

input_data_prev = 0
published = False
payload_data = 1

client = paho.mqtt.client.Client(client_id="", clean_session=True, userdata=None, protocol=mqtt.MQTTv311, transport="tcp")

client.username_pw_set(username, password)
res = client.connect(hostname, port=1883, keepalive=60, bind_address="")
print(res)

while(1):
  input_data = dev.read_register(REG_INPUT, functioncode=MODBUS_READ_INPUT)
  
  if (input_data != 0):
    if (not published ):

      client.publish("geiger/data", payload=payload_data, qos=0, retain=False)
      published = True
      if (payload_data):
        payload_data = 0
      else:
        payload_data = 1
      print(payload_data)

  else:
    published = False
    


  



