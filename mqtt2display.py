import serial
import paho
import paho.mqtt.client as mqtt
import paho.mqtt.subscribe as subscribe
import time
import threading
import configparser
import os
import minimalmodbus
import threading
import random

COM               = "/dev/ttyUSB0"
slave_address     = 7
MODBUS_READ_INPUT = 0x4
REG_INPUT         = 51
MODBUS_MODE       = 'rtu'

candle_dev = minimalmodbus.Instrument(COM, slave_address, mode=MODBUS_MODE)
candle_dev.serial.baudrate = 115200

def off_all_candles():
    candle_dev.write_bit(8,0, functioncode=5)
    candle_dev.write_bit(9,0, functioncode=5)
    candle_dev.write_bit(10,0, functioncode=5)
    candle_dev.write_bit(11,0, functioncode=5)
    candle_dev.write_bit(12,0, functioncode=5)
    candle_dev.write_bit(13,0, functioncode=5)
    candle_dev.write_bit(14,0, functioncode=5)
    candle_dev.write_bit(15,0, functioncode=5)

def enable_all_candles():
    candle_dev.write_bit(0,1, functioncode=5)
    candle_dev.write_bit(1,1, functioncode=5)
    candle_dev.write_bit(2,1, functioncode=5)
    candle_dev.write_bit(3,1, functioncode=5)
    candle_dev.write_bit(4,1, functioncode=5)
    candle_dev.write_bit(5,1, functioncode=5)
    candle_dev.write_bit(6,1, functioncode=5)
    candle_dev.write_bit(7,1, functioncode=5)



enable_all_candles()


dat_to_disp = b''


def subs():

    print('geiger_lol4')

    test = subscribe.callback(print_mqqt_data, 'geiger/+', qos=0, userdata=None, hostname='weather.shsh.io',
    client_id="", keepalive=60, will=None, tls=None,
    protocol=mqtt.MQTTv311)

ser = serial.Serial('/dev/ttyUSB1')  # open serial port
print(ser.name)         # check which port was really used
ser.write(b'hello')     # write a string
ser.write(b'\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r')



def print_mqqt_data(client, userdata, message):
    global dat_to_disp
    
    data = int(message.payload.decode("utf-8"))

    #print(len(message.payload))
    dat_to_disp_len = len(dat_to_disp)
    for x in range(dat_to_disp_len):
      ser.write(b'\r')

    padding = 5 - len(message.payload)
    #print("padding", padding)
    padded_payload = b"".join([b" " for _ in range(padding)]) + message.payload
    dat_to_disp = padded_payload + b':' + dat_to_disp
    dat_to_disp = dat_to_disp[:20] 
    #print(dat_to_disp)
    ser.write(dat_to_disp)
    dat_to_disp = message.payload + b':' + dat_to_disp[6:]


def setInterval(interval):
    def decorator(function):
        def wrapper(*args, **kwargs):
            stopped = threading.Event()

            def loop(): # executed in another thread
                while not stopped.wait(interval): # until stopped
                    function(*args, **kwargs)

            t = threading.Thread(target=loop)
            t.daemon = True # stop if the program exits
            t.start()
            return stopped
        return wrapper
    return decorator


    
    # print(message.payload)
    # for x in range(dat_len):
    #   ser.write(x)
    #   ser.write(b'\\')
    # #ser.write(b'\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r\r')
    # #ser.write(message.payload)
    # ser.write(b'\\')
    
    # # for x in range(dat_len):
    # #   ser.write(b'\r')
    # # for x in range(10-dat_len):
    # #   ser.write(b' ')
    # ser.write(message.payload)     # write a string


@setInterval(3)
def update_candles():
  off_all_candles()
  rand = random.randint(8,15)
  candle_dev.write_bit(rand,1, functioncode=5)
  print(rand)
  

update_candles()

  



   



s = threading.Thread(name='subs', target=subs)

s.start()


