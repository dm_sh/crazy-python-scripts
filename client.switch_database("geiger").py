client.switch_database("geiger")
geiger_data = client.query('select elapsed(value, 10ms), first(value) FROM mqtt_consumer   WHERE time > now() - 1d ')
data_size =  client.query('SELECT COUNT("value") FROM mqtt_consumer   WHERE time > now() - 1d ')
geiger_points = list(geiger_data.get_points(measurement='mqtt_consumer'))
data_size_l = list(data_size.get_points(measurement='mqtt_consumer'))
bits = []
four_bit_data = []
for x in range(1, (data_size_l[0]['count'])-2):
   
     if  (geiger_points[x]['elapsed']) > (geiger_points[x+1]['elapsed']):
        bits.append(1)
     else:
        bits.append(0)

print(bits)

print("bits_len=",len(bits)//4)
for x in range(0, len(bits)//4):
    bit_data = (bits[-(1+4*x)]) | (bits[-(2+4*x)] << 1) | (bits[-(3+4*x)] << 2) | (bits[-(4+4*x)] << 3)
    four_bit_data.append(bit_data)