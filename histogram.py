from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from influxdb import InfluxDBClient

client = InfluxDBClient(host='54.188.48.101', port=8086)

client.switch_database("geiger")
geiger_data = client.query('select elapsed(value, 10ms), first(value) FROM mqtt_consumer   WHERE time > now() - 2h')
data_size =  client.query('SELECT COUNT("value") FROM mqtt_consumer   WHERE time > now() - 2h ')
geiger_points = list(geiger_data.get_points(measurement='mqtt_consumer'))
data_size_l = list(data_size.get_points(measurement='mqtt_consumer'))
bits = []
four_bit_data = []

median = np.median(geiger_points)

print("median={0}".format(mean,median))

for x in range(1, (data_size_l[0]['count'])-2, 2):
   
     if  (geiger_points[x]['elapsed']) > (geiger_points[x+1]['elapsed']):
        bits.append(1)
     elif(geiger_points[x]['elapsed']) < (geiger_points[x+1]['elapsed']):
        bits.append(0)


for x in range(0, len(bits)//4):
    bit_data = (bits[-(1+4*x)]) | (bits[-(2+4*x)] << 1) | (bits[-(3+4*x)] << 2) | (bits[-(4+4*x)] << 3)
    four_bit_data.append(bit_data)

print(bits)
print("four_bit_data = ",four_bit_data)
print("four_bit_data len= ",len(four_bit_data))

hist, bins = np.histogram(four_bit_data, bins=15)
plt.hist(four_bit_data, 15)

plt.show()